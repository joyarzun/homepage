import React from "react";
import { shallow, mount, render } from "enzyme";

import App from "../src/shared/App";
import Title from "../src/client/Title";

describe("<App />", () => {
  it("renders <App /> components", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(Title).length).toBe(1);
  });
});
