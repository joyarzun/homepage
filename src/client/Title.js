import React from "react";
import styles from "../shared/App.css";

const title = props => (
  <div className="row">
    <div className="col">
      <h1 className={styles.head1}>{props.title}</h1>
    </div>
  </div>
);

export default title;
