import { connect } from "react-redux";
import { change } from "./actions";
import List from "./List";

const mapStateToProps = state => {
  return {
    list: state.changeList
  };
};

const listContainer = connect(mapStateToProps)(List);

export default listContainer;
