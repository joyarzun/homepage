export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";

export const increment = size => {
  return { type: INCREMENT, size };
};
export const decrement = size => {
  return { type: DECREMENT, size };
};
