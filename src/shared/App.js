import React from "react";
import styles from "./App.css";
import Counter from "../client/counter/counterContainer";
import List from "../client/list/listContainer";
import Title from "../client/Title";

const App = props => (
  <div className={`container ${styles.root}`}>
    <Title title={props.title} />
    <List />
    <Counter />
  </div>
);

export default App;
