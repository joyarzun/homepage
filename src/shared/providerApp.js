import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import App from "./App";
import reducer from "./reducers";

const providerApp = data => {
  const preloadState = {
    counterCalc: 0,
    changeList: data
  };

  const enhancerForUniversal =
    (typeof window !== "undefined" &&
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__()) ||
    applyMiddleware();

  const store = createStore(reducer, preloadState, enhancerForUniversal);

  return (
    <Provider store={store}>
      <App title="Title" />
    </Provider>
  );
};

export default providerApp;
